import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Customer Landing
CustomerLanding_node1709760347648 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/customer/landing/"],
        "recurse": True,
    },
    transformation_ctx="CustomerLanding_node1709760347648",
)

# Script generated for node Share With Research
SqlQuery0 = """
select * from cl where sharewithresearchasofdate is not null
"""
ShareWithResearch_node1709763576832 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={"cl": CustomerLanding_node1709760347648},
    transformation_ctx="ShareWithResearch_node1709763576832",
)

# Script generated for node Customer Trusted
CustomerTrusted_node1709760635126 = glueContext.write_dynamic_frame.from_options(
    frame=ShareWithResearch_node1709763576832,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/customer/trusted/",
        "compression": "snappy",
        "partitionKeys": [],
    },
    transformation_ctx="CustomerTrusted_node1709760635126",
)

job.commit()
