import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Customer Curated
CustomerCurated_node1709772564722 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/customer/curated/"],
        "recurse": True,
    },
    transformation_ctx="CustomerCurated_node1709772564722",
)

# Script generated for node Step Trainer Landing
StepTrainerLanding_node1709772566498 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/step_trainer/landing/"],
        "recurse": True,
    },
    transformation_ctx="StepTrainerLanding_node1709772566498",
)

# Script generated for node Step Trainer of Consented Users
SqlQuery0 = """
SELECT * FROM stl WHERE serialnumber IN (SELECT serialnumber FROM ct);
"""
StepTrainerofConsentedUsers_node1709772572214 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={
        "stl": StepTrainerLanding_node1709772566498,
        "ct": CustomerCurated_node1709772564722,
    },
    transformation_ctx="StepTrainerofConsentedUsers_node1709772572214",
)

# Script generated for node Step Trainer Trusted
StepTrainerTrusted_node1709772574063 = glueContext.write_dynamic_frame.from_options(
    frame=StepTrainerofConsentedUsers_node1709772572214,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/step_trainer/trusted/",
        "partitionKeys": [],
    },
    transformation_ctx="StepTrainerTrusted_node1709772574063",
)

job.commit()
