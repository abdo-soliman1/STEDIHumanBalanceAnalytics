CREATE EXTERNAL TABLE IF NOT EXISTS `step_trainer`.`machine_learning_curated` (
  `serialnumber` string,
  `user_email` string,
  `x` double,
  `y` double,
  `z` double,
  `distanceFromObject` int,
  `ts` bigint
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
  'ignore.malformed.json' = 'FALSE',
  'dots.in.keys' = 'FALSE',
  'case.insensitive' = 'TRUE',
  'mapping' = 'TRUE'
)
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat' OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 's3://stedi-step-trainer-lakehouse/machine_learning/curated/'
TBLPROPERTIES ('classification' = 'json');
