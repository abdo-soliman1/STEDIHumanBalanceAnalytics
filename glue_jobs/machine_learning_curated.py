import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Step Trainer Trusted
StepTrainerTrusted_node1709773621761 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/step_trainer/trusted/"],
        "recurse": True,
    },
    transformation_ctx="StepTrainerTrusted_node1709773621761",
)

# Script generated for node Accelerometer Trusted
AccelerometerTrusted_node1709773623562 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/accelerometer/trusted"],
        "recurse": True,
    },
    transformation_ctx="AccelerometerTrusted_node1709773623562",
)

# Script generated for node JOIN Step Trainer & Accelerometer
SqlQuery0 = """
SELECT
    stt.serialnumber as serialnumber,
    act.user as user_email,
    act.x as x,
    act.y as y,
    act.z as z,
    stt.distanceFromObject as distanceFromObject,
    act.timestamp as ts
FROM act JOIN stt
ON act.timestamp = stt.sensorReadingTime
"""
JOINStepTrainerAccelerometer_node1709774321113 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={
        "act": AccelerometerTrusted_node1709773623562,
        "stt": StepTrainerTrusted_node1709773621761,
    },
    transformation_ctx="JOINStepTrainerAccelerometer_node1709774321113",
)

# Script generated for node Machine Learning Curated
MachineLearningCurated_node1709774430267 = glueContext.write_dynamic_frame.from_options(
    frame=JOINStepTrainerAccelerometer_node1709774321113,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/machine_learning/curated/",
        "partitionKeys": [],
    },
    transformation_ctx="MachineLearningCurated_node1709774430267",
)

job.commit()
