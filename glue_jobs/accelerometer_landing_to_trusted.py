import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Customer Trusted
CustomerTrusted_node1709764175618 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/customer/trusted/"],
        "recurse": True,
    },
    transformation_ctx="CustomerTrusted_node1709764175618",
)

# Script generated for node Accelerometer Landing
AccelerometerLanding_node1709764178140 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/accelerometer/landing/"],
        "recurse": True,
    },
    transformation_ctx="AccelerometerLanding_node1709764178140",
)

# Script generated for node Join Research Customers
JoinResearchCustomers_node1709765622849 = Join.apply(
    frame1=CustomerTrusted_node1709764175618,
    frame2=AccelerometerLanding_node1709764178140,
    keys1=["email"],
    keys2=["user"],
    transformation_ctx="JoinResearchCustomers_node1709765622849",
)

# Script generated for node Drop Customer Fields
DropCustomerFields_node1709765650272 = DropFields.apply(
    frame=JoinResearchCustomers_node1709765622849,
    paths=[
        "phone",
        "email",
        "lastUpdateDate",
        "shareWithFriendsAsOfDate",
        "customerName",
        "registrationDate",
        "shareWithResearchAsOfDate",
        "shareWithPublicAsOfDate",
        "birthDay",
        "serialNumber",
    ],
    transformation_ctx="DropCustomerFields_node1709765650272",
)

# Script generated for node Accelerometer Trusted
AccelerometerTrusted_node1709764824261 = glueContext.write_dynamic_frame.from_options(
    frame=DropCustomerFields_node1709765650272,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/accelerometer/trusted/",
        "partitionKeys": [],
    },
    transformation_ctx="AccelerometerTrusted_node1709764824261",
)

job.commit()
