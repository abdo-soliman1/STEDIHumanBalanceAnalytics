# STEDI Human Balance Analytics

Building a data lakehouse solution for sensor data that trains a machine learning model to detect Steps.
## Steps

- Setting Up Cloud Shell
- Creating S3 bucket
- Loading data into landing zone
- Glue Jobs
## Setting Up Cloud Shell

one of AWS best features is the ability to use command line to create and manage your AWS resources. the AWS command line interface could installed on any machine or  we can open cloud shell console by clicking on the cloud shell icon in AWS Console Home Page like the following:

![Cloud Shell](./Assets/cloudshell.png)

this will spin up  a virtual machine for us to allow to use AWS via command line, it will also persist any data we add to it.

For this project we use a git repo to keep our project data and files so once the shell opens we start by cloning our git repo using the following command:

```bash
git clone https://gitlab.com/abdo-soliman1/STEDIHumanBalanceAnalytics.git
```

once the repo is cloned we should we will find a directory inside the repo called data containing all our landing data like the following:

![Cloning Repo](./Assets/cloning_repo.jpg)
## Creating S3 Bucket

Now that we cloned our repo and downloaded our landing data into AWS cloud shell we can create an S3 bucket and load the data into the landing zone. to create an S3 bucket we can use the following command via the Cloud Shell:

```bash
aws s3 mb s3://stedi-step-trainer-lakehouse
```

Now that we created the bucket we need to create a VPC endpoint for ec2 based services to be able to access the bucket to do this we need the following information:

- VPC ID
- Route Table ID
### Finding VPC ID

to get the VPC ID we use the following command

```bash
aws ec2 describe-vpcs
```

which should return the details of your VPCs of which we need the **VpcId** like the following:

![VPC ID](./Assets/vpcid.png)
### Finding Route Table ID

to get the route table id we use the following command:

```bash
aws ec2 describe-route-tables
```

From the result we can find the route table id like the following:

![Route Table ID](./Assets/route_table_id.png)
### Creating VPC Endpoint

to create a VPC end point we use the following command:

```bash
aws ec2 create-vpc-endpoint --vpc-id vpc-0a80aa0fa0b218c99 --service-name com.amazonaws.us-east-1.s3 --route-table-ids rtb-09043f16a942ecc03
```

this should create an endpoint for any ec2 based service (in our case Glue) to access our s3 bucket that we just created like the following:

![VPC S3 Endpoint](./Assets/vpc_s3_endpoint.jpg)
### Creating Glue IAM Role

Now that we created the endpoint **Glue** can reach and interact with our S3 bucket, however, so far it doesn't have the necessary privileges to access and modify the data in S3 bucket. To give **Glue** access to access our S3 bucket we need to create an **IAM Role** to grant it the necessary permissions using the following command:

```bash
aws iam create-role --role-name sstlh-glue-service-role --assume-role-policy-document '{
	"Version": "2012-10-17",
	"Statement": [{
		"Effect": "Allow",
		"Principal": {
			"Service": "glue.amazonaws.com"
		},
		"Action": "sts:AssumeRole"
	}]
}'
```

we get the following result:

![Glue IAM Role](./Assets/glue_iam_role.jpg)
### Grant Glue Privileges on the S3 Bucket

To grant Glue access to our S3 bucket we need to add the following policy to **IAM Role**:

```bash
aws iam put-role-policy --role-name sstlh-glue-service-role --policy-name S3Access --policy-document '{
    "Version": "2012-10-17",
    "Statement": [
        {
	        "Sid": "ListObjectsInBucket",
	        "Effect": "Allow",
	        "Action": ["s3:ListBucket"],
	        "Resource": ["arn:aws:s3:::stedi-step-trainer-lakehouse"]
        },
        {
	        "Sid": "AllObjectActions",
	        "Effect": "Allow",
	        "Action": "s3:*Object",
	        "Resource": ["arn:aws:s3:::stedi-step-trainer-lakehouse/*"]
        }
    ]
}'
```

This policy allow your Glue job read/write/delete access to the bucket. we can validate that our policy was created by listing it using the following command:

```bash
aws iam get-role-policy --role-name sstlh-glue-service-role --policy-name S3Access
```

which gives us the following result:

![IAM S3 Access Policy](./Assets/iam_s3_access_policy.jpg)
### Glue Policy

Last, we need to give Glue access to data in special S3 buckets used for Glue configuration, and several other resources. We use the following command to add a policy for general access needed by Glue:

```bash
aws iam put-role-policy --role-name sstlh-glue-service-role --policy-name GlueAccess --policy-document '{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "glue:*",
                "s3:GetBucketLocation",
                "s3:ListBucket",
                "s3:ListAllMyBuckets",
                "s3:GetBucketAcl",
                "ec2:DescribeVpcEndpoints",
                "ec2:DescribeRouteTables",
                "ec2:CreateNetworkInterface",
                "ec2:DeleteNetworkInterface",
                "ec2:DescribeNetworkInterfaces",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeSubnets",
                "ec2:DescribeVpcAttribute",
                "iam:ListRolePolicies",
                "iam:GetRole",
                "iam:GetRolePolicy",
                "cloudwatch:PutMetricData"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:CreateBucket",
                "s3:PutBucketPublicAccessBlock"
            ],
            "Resource": [
                "arn:aws:s3:::aws-glue-*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::aws-glue-*/*",
                "arn:aws:s3:::*/*aws-glue-*/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::crawler-public*",
                "arn:aws:s3:::aws-glue-*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:AssociateKmsKey"
            ],
            "Resource": [
                "arn:aws:logs:*:*:/aws-glue/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:CreateTags",
                "ec2:DeleteTags"
            ],
            "Condition": {
                "ForAllValues:StringEquals": {
                    "aws:TagKeys": [
                        "aws-glue-service-resource"
                    ]
                }
            },
            "Resource": [
                "arn:aws:ec2:*:*:network-interface/*",
                "arn:aws:ec2:*:*:security-group/*",
                "arn:aws:ec2:*:*:instance/*"
            ]
        }
    ]
}'
```

we can validate that our policy was created by listing it using the following command:

```bash
aws iam get-role-policy --role-name sstlh-glue-service-role --policy-name GlueAccess
```

which gives us the following result:

![IAM Glue Access Policy](./Assets/iam_glue_access_policy.jpg)
### Moving Landing Data to S3

Now that we have created our s3 bucket, VPC endpoint, and IAM Role, we can copy the landing data into landing zone in our S3 bucket. We have 3 tables for the landing zone:

| Table Name    |        Path In Repo        |
| ------------- | :------------------------: |
| Customer      |   data/customer/landing    |
| Accelerometer | data/accelerometer/landing |
| StepTrainer   | data/step_trainer/landing  |
we navigate into the Repo directory using the following command:

```bash
cp ./STEDIHumanBalanceAnalytics
```

we use the following commands to copy data from the cloud console to our S3 bucket landing zone:

```bash
# to copy customer landing data
aws s3 cp --recursive ./data/customer/landing/ s3://stedi-step-trainer-lakehouse/customer/landing/

# to copy accelerometer landing data
aws s3 cp --recursive ./data/accelerometer/landing/ s3://stedi-step-trainer-lakehouse/accelerometer/landing/

# to copy step_trainer landing data
aws s3 cp --recursive ./data/step_trainer/landing/ s3://stedi-step-trainer-lakehouse/step_trainer/landing/
```

<span style="color: red;">Please Note: that the slash at the end of the S3 URL is necessary so s3 considers the path is a directory and moves the files into it.</span>

We should get the following result indicating that all the files were uploaded successfully:

![Uploading Landing Data to S3](./Assets/uploading_landing_data_to_s3.jpg)
## Landing Zone
### Setting Up Athena

To create landing tables in Glue we search for Glue in AWS Dashboard and select Glue Data Catalog. Then we select Databases from the side menu and we add a database and create a database called **step_trainer**

![Creating Glue Database](./Assets/creating_glue_database.jpg)

To create the landing tables and get the DDL Queries, we use **AWS Athena**. First we set the query result location to our S3 Bucket like the following:

![Athena S3 Config](./Assets/athena_s3_config.jpg)
### Customer Landing

Now we go to the tables section and create **customer_landing** table with the following properties and schema:

![Customer Landing Table](./Assets/customer_landing_table.png)

Then we get the following DDL Queries for creating the table which we save to in our repo under the directory **ddl_queries**:

```sql
CREATE EXTERNAL TABLE IF NOT EXISTS `step_trainer`.`customer_landing` (
	`customerName` string,
	`email` string,
	`phone` string,
	`birthDay` string,
	`serialNumber` string,
	`registrationDate` bigint,
	`lastUpdateDate` bigint,
	`shareWithResearchAsOfDate` bigint,
	`shareWithPublicAsOfDate` bigint,
	`shareWithFriendsAsOfDate` bigint
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
	'ignore.malformed.json' = 'FALSE',
	'dots.in.keys' = 'FALSE',
	'case.insensitive' = 'TRUE',
	'mapping' = 'TRUE'
)
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat' OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 's3://stedi-step-trainer-lakehouse/customer/landing/'
TBLPROPERTIES ('classification' = 'json');
```

we run the following query to investigate the data:

```sql
select * from customer_landing
```

which gives us the following query result:

![Customer Landing Table Query](./Assets/customer_landing_table_query.jpg)
### Accelerometer Landing

We create **accelerometer_landing** table with the following properties and schema:

![Accelerometer Landing Table](./Assets/accelerometer_landing_table.png)

Then we get the following DDL Queries for creating the table which we save to in our repo under the directory **ddl_queries**:

```sql
CREATE EXTERNAL TABLE IF NOT EXISTS `step_trainer`.`accelerometer_landing` (
	`user` string,
	`timeStamp` bigint,
	`x` double,
	`y` double,
	`z` double
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
	'ignore.malformed.json' = 'FALSE',
	'dots.in.keys' = 'FALSE',
	'case.insensitive' = 'TRUE',
	'mapping' = 'TRUE'
)
STORED AS INPUTFORMAT 'org.apache.hadoop.mapred.TextInputFormat' OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 's3://stedi-step-trainer-lakehouse/accelerometer/landing/'
TBLPROPERTIES ('classification' = 'json');
```

we run the following query to investigate the data:

```sql
select * from accelerometer_landing
```

which gives us the following result:

![Accelerometer Landing table Query](./Assets/accelerometer_landing_table_query.jpg)
## Glue Jobs
### Customer Landing to Trusted

We start by creating a job **customer_landing_to_trusted** which reads data from **customer_landing table** and filter it to get only the data were the flag **shareWithResearchAsOfDate is not null**. Then store the result in the trusted zone.

First we create the S3 Bucket data source to read the data with the following properties:

![Customer Landing to Trusted Source](./Assets/customer_landing_to_trusted_source.jpg)

The second step is we add an SQL node with the following properties:

![Customer Landing to Trusted Filter](./Assets/customer_landing_to_trusted_filter.png)

Finally, we add a S3 Data Target Node with the following properties:

![Customer Landing to Trusted Target](./Assets/customer_landing_to_trusted_target.jpg)

Now the job is done however to be able to run it we need to set the following properties in the Job Details tab:

![Customer Landing to Trusted Job Details](./Assets/customer_landing_to_trusted_job_details.png)

<span style="color: red;">Please Note: these two properties will be set for all glue jobs used in this project.</span>

we can generate this job as a python script by going to the script tab in Glue Studio and downloading the python script which for this job gives us the following script:

```python
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Customer Landing
CustomerLanding_node1709760347648 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/customer/landing/"],
        "recurse": True,
    },
    transformation_ctx="CustomerLanding_node1709760347648",
)

# Script generated for node Share With Research
SqlQuery0 = """
select * from cl where sharewithresearchasofdate is not null
"""
ShareWithResearch_node1709763576832 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={"cl": CustomerLanding_node1709760347648},
    transformation_ctx="ShareWithResearch_node1709763576832",
)

# Script generated for node Customer Trusted
CustomerTrusted_node1709760635126 = glueContext.write_dynamic_frame.from_options(
    frame=ShareWithResearch_node1709763576832,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/customer/trusted/",
        "compression": "snappy",
        "partitionKeys": [],
    },
    transformation_ctx="CustomerTrusted_node1709760635126",
)

job.commit()
```

the job script is saved in the repo in the **glue_jobs** directory. Now we can run our job by clicking the **Run** in the visual mode at the top right. once we click the job runs and we get the following notification to see the run details:

![Customer Landing to Trusted Job Run](./Assets/customer_landing_to_trusted_job_run.png)

In the run details you will find the job id and logs like the following:

![Customer Landing to Trusted Job Run Details](./Assets/customer_landing_to_trusted_job_run_details.png)

After the job runs and complete the status should updated automatically to successful like the following:

![Customer Landing to Trusted Job run Successful](./Assets/customer_landing_to_trusted_job_run_successful.png)

Finally to make sure the job ran successfully we go to **Athena** create the table **customer_trusted** and query it like the following:

![Customer Trusted Table Query](./Assets/customer_trusted_table_query.jpg)
### Accelerometer Landing to Trusted

In this section we create the job **accelerometer_landing_to_trusted** which reads data from **accelerometer_landing table** and filter it to get only the data were the user consented to share data for research. Then store the result in the trusted zone.

First we create the S3 Bucket data source to read the data from **customer_trusted table** with the following properties:

![Accelerometer Landing to Trusted Customer Trusted Source](./Assets/accelerometer_landing_to_trusted_customer_trusted_source.jpg)

Then we create the S3 Bucket data source to read the data from **accelerometer_landing table** with the following properties:

![Accelerometer Landing to Trusted Accelerometer Landing Source](./Assets/accelerometer_landing_to_trusted_accelerometer_landing_source.jpg)

Now we join both tables to get only accelerometer data for user to consented to share data for research by using the following Join Node:

![Accelerometer Landing to Trusted Transform Join](./Assets/accelerometer_landing_to_trusted_transform_join.jpg)

The Join results contains columns from both tables so we need to drop Customer table columns using the following Drop Fields Node:

![Accelerometer Landing to Trusted Transform Drop Customer Fields](./Assets/accelerometer_landing_to_trusted_transform_drop_customer_fields.jpg)

Finally write the data to accelerometer trusted zone using the following S3 target Node:

![Accelerometer Landing to Trusted Target](./Assets/accelerometer_landing_to_trusted_target.jpg)

We save the job and we get the following job script which is saved in the repo in the **glue_jobs** directory:

```python
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Customer Trusted
CustomerTrusted_node1709764175618 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/customer/trusted/"],
        "recurse": True,
    },
    transformation_ctx="CustomerTrusted_node1709764175618",
)

# Script generated for node Accelerometer Landing
AccelerometerLanding_node1709764178140 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/accelerometer/landing/"],
        "recurse": True,
    },
    transformation_ctx="AccelerometerLanding_node1709764178140",
)

# Script generated for node Join Research Customers
JoinResearchCustomers_node1709765622849 = Join.apply(
    frame1=CustomerTrusted_node1709764175618,
    frame2=AccelerometerLanding_node1709764178140,
    keys1=["email"],
    keys2=["user"],
    transformation_ctx="JoinResearchCustomers_node1709765622849",
)

# Script generated for node Drop Customer Fields
DropCustomerFields_node1709765650272 = DropFields.apply(
    frame=JoinResearchCustomers_node1709765622849,
    paths=[
        "phone",
        "email",
        "lastUpdateDate",
        "shareWithFriendsAsOfDate",
        "customerName",
        "registrationDate",
        "shareWithResearchAsOfDate",
        "shareWithPublicAsOfDate",
        "birthDay",
        "serialNumber",
    ],
    transformation_ctx="DropCustomerFields_node1709765650272",
)

# Script generated for node Accelerometer Trusted
AccelerometerTrusted_node1709764824261 = glueContext.write_dynamic_frame.from_options(
    frame=DropCustomerFields_node1709765650272,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/accelerometer/trusted/",
        "partitionKeys": [],
    },
    transformation_ctx="AccelerometerTrusted_node1709764824261",
)

job.commit()
```

the job script is saved in the repo in the **glue_jobs** directory. now we can simple save the job and run it. Once the job is finished we go to **Athena** create the table  **accelerometer_trusted** and query it to make sure the job completed successfully like the following:

![Accelerometer Trusted Table Query](./Assets/accelerometer_trusted_table_query.jpg)
## Customer Trusted to Curated

In this section we create the job **customer_trusted_to_curated** which reads data from **customer_trusted table** and filter it to get only customers who consented to share data for research and has accelerometer data. Then store the result in the curated zone.

First we create the S3 Bucket data source to read the data from **customer_trusted table** with the following properties:

![customer Trusted to Curated Source Customer Trusted](./Assets/customer_trusted_to_curated_source_customer_trusted.jpg)

Then we create the S3 Bucket data source to read the data from **accelerometer_trusted table** with the following properties:

![Customer Trusted to Curated Source Accelerometer Trusted](./Assets/customer_trusted_to_curated_source_accelerometer_trusted.jpg)

Now we filter both tables to get only customer data who have accelerometer data and have agreed to share their data for research using the following SQL Node:

![Customer Trusted to Curated Customer Trans Consent With ACC Data](./Assets/customer_trusted_to_curated_customer_trans_consent_with_acc_data.png)

Finally write the data to customer curated zone using the following S3 target Node:

![Customer Trusted to Curated Target](./Assets/customer_trusted_to_curated_target.jpg)

We save the job and we get the following job script which is saved in the repo in the **glue_jobs** directory:

```python
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Customer Trusted
CustomerTrusted_node1709769827646 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/customer/trusted/"],
        "recurse": True,
    },
    transformation_ctx="CustomerTrusted_node1709769827646",
)

# Script generated for node Accelerometer Trusted
AccelerometerTrusted_node1709769902174 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/accelerometer/trusted/"],
        "recurse": True,
    },
    transformation_ctx="AccelerometerTrusted_node1709769902174",
)

# Script generated for node Customer Consent With ACC Data
SqlQuery0 = """
SELECT * FROM ct WHERE email in (SELECT DISTINCT user FROM act)
"""
CustomerConsentWithACCData_node1709769943282 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={
        "ct": CustomerTrusted_node1709769827646,
        "act": AccelerometerTrusted_node1709769902174,
    },
    transformation_ctx="CustomerConsentWithACCData_node1709769943282",
)

# Script generated for node Customer Curated
CustomerCurated_node1709770010699 = glueContext.write_dynamic_frame.from_options(
    frame=CustomerConsentWithACCData_node1709769943282,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/customer/curated/",
        "partitionKeys": [],
    },
    transformation_ctx="CustomerCurated_node1709770010699",
)

job.commit()
```

the job script is saved in the repo in the **glue_jobs** directory. now we can simple save the job and run it. Once the job is finished we go to **Athena** create the table  **customer_curated** and query it to make sure the job completed successfully like the following:

![Customer Curated Table Query](./Assets/customer_curated_table_query.jpg)
### Step Trainer Landing to Trusted

In this section we create the job **step_trainer_landing_to_trusted** which reads data from **step_trainer_landing table** and filter it to get the Step Trainer Records data for customers who have accelerometer data and have agreed to share their data for research (customers_curated). Then store the result in the trusted zone.

First we create the S3 Bucket data source to read the data from **step_trainer_landing table** with the following properties:

![Step Trainer Landing to Trusted Source Step Trainer Landing](./Assets/step_trainer_landing_to_trusted_source_step_trainer_landing.jpg)

Then we create the S3 Bucket data source to read the data from **customer_curated table** with the following properties:

![Step Trainer Landing to Trusted Source Customer Curated](./Assets/step_trainer_landing_to_trusted_source_customer_curated.jpg)

Now we filter both tables to get only step trainer data of users who have accelerometer data and have agreed to share their data for research using the following SQL Node:

![Step Trainer Landing to Trusted Trans Step Trainer of Consented Users](./Assets/step_trainer_landing_to_trusted_trans_step_trainer_of_consented_users.jpg)

Finally write the data to step_trainer trusted zone using the following S3 target Node:

![Step Trainer Landing to Trusted Target](./Assets/step_trainer_landing_to_trusted_target.jpg)

We save the job and we get the following job script which is saved in the repo in the **glue_jobs** directory:

```python
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Customer Curated
CustomerCurated_node1709772564722 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/customer/curated/"],
        "recurse": True,
    },
    transformation_ctx="CustomerCurated_node1709772564722",
)

# Script generated for node Step Trainer Landing
StepTrainerLanding_node1709772566498 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/step_trainer/landing/"],
        "recurse": True,
    },
    transformation_ctx="StepTrainerLanding_node1709772566498",
)

# Script generated for node Step Trainer of Consented Users
SqlQuery0 = """
SELECT * FROM stl WHERE serialnumber IN (SELECT serialnumber FROM ct);
"""
StepTrainerofConsentedUsers_node1709772572214 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={
        "stl": StepTrainerLanding_node1709772566498,
        "ct": CustomerCurated_node1709772564722,
    },
    transformation_ctx="StepTrainerofConsentedUsers_node1709772572214",
)

# Script generated for node Step Trainer Trusted
StepTrainerTrusted_node1709772574063 = glueContext.write_dynamic_frame.from_options(
    frame=StepTrainerofConsentedUsers_node1709772572214,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/step_trainer/trusted/",
        "partitionKeys": [],
    },
    transformation_ctx="StepTrainerTrusted_node1709772574063",
)

job.commit()
```

the job script is saved in the repo in the **glue_jobs** directory. now we can simple save the job and run it. Once the job is finished we go to **Athena** create the table **step_trainer_trusted** and query it to make sure the job completed successfully like the following:

![Step Trainer Trusted Table Query](./Assets/step_trainer_trusted_table_query.jpg)
### Machine Learning Curated

In this section we create the job **machine_learning_curated** which reads data from **accelerometer_trusted table** and joins it with **step_trainer_trusted table** to get machine learning data of the customers who consented to share data for research and has accelerometer data. Then store the result in the curated zone.

First we create the S3 Bucket data source to read the data from **accelerometer_trusted table** with the following properties:

![Machine Learning Curated Source Accelerometer Trusted](./Assets/machine_learning_curated_source_accelerometer_trusted.jpg)

Then we create the S3 Bucket data source to read the data from **step_trainer_trusted table** with the following properties:

![Machine Learning Curated Source Step Trainer Trusted](./Assets/machine_learning_curated_source_step_trainer_trusted.jpg)

Now we join both tables to get the machine learning data of the user who have agreed to share their data for research using the following SQL Node:

![Machine Learning Curated Trans Join Step Trainer and Accelerometer](./Assets/machine_learning_curated_trans_join_step_trainer_and_accelerometer.jpg)

Finally write the data to machine_learning curated zone using the following S3 target Node:

![Machine Learning Curated Target](./Assets/machine_learning_curated_target.jpg)

We save the job and we get the following job script which is saved in the repo in the **glue_jobs** directory:

```python
import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Step Trainer Trusted
StepTrainerTrusted_node1709773621761 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/step_trainer/trusted/"],
        "recurse": True,
    },
    transformation_ctx="StepTrainerTrusted_node1709773621761",
)

# Script generated for node Accelerometer Trusted
AccelerometerTrusted_node1709773623562 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/accelerometer/trusted"],
        "recurse": True,
    },
    transformation_ctx="AccelerometerTrusted_node1709773623562",
)

# Script generated for node JOIN Step Trainer & Accelerometer
SqlQuery0 = """
SELECT
    stt.serialnumber as serialnumber,
    act.user as user_email,
    act.x as x,
    act.y as y,
    act.z as z,
    stt.distanceFromObject as distanceFromObject,
    act.timestamp as ts
FROM act JOIN stt
ON act.timestamp = stt.sensorReadingTime
"""
JOINStepTrainerAccelerometer_node1709774321113 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={
        "act": AccelerometerTrusted_node1709773623562,
        "stt": StepTrainerTrusted_node1709773621761,
    },
    transformation_ctx="JOINStepTrainerAccelerometer_node1709774321113",
)

# Script generated for node Machine Learning Curated
MachineLearningCurated_node1709774430267 = glueContext.write_dynamic_frame.from_options(
    frame=JOINStepTrainerAccelerometer_node1709774321113,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/machine_learning/curated/",
        "partitionKeys": [],
    },
    transformation_ctx="MachineLearningCurated_node1709774430267",
)

job.commit()
```

the job script is saved in the repo in the **glue_jobs** directory. now we can simple save the job and run it. Once the job is finished we go to **Athena** create the table **machine_learning_curated** and query it to make sure the job completed successfully like the following:

![Machine Learning Curated Table Query](./Assets/machine_learning_curated_table_query.jpg)
