import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node Customer Trusted
CustomerTrusted_node1709769827646 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/customer/trusted/"],
        "recurse": True,
    },
    transformation_ctx="CustomerTrusted_node1709769827646",
)

# Script generated for node Accelerometer Trusted
AccelerometerTrusted_node1709769902174 = glueContext.create_dynamic_frame.from_options(
    format_options={"multiline": False},
    connection_type="s3",
    format="json",
    connection_options={
        "paths": ["s3://stedi-step-trainer-lakehouse/accelerometer/trusted/"],
        "recurse": True,
    },
    transformation_ctx="AccelerometerTrusted_node1709769902174",
)

# Script generated for node Customer Consent With ACC Data
SqlQuery0 = """
SELECT * FROM ct WHERE email in (SELECT DISTINCT user FROM act)
"""
CustomerConsentWithACCData_node1709769943282 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={
        "ct": CustomerTrusted_node1709769827646,
        "act": AccelerometerTrusted_node1709769902174,
    },
    transformation_ctx="CustomerConsentWithACCData_node1709769943282",
)

# Script generated for node Customer Curated
CustomerCurated_node1709770010699 = glueContext.write_dynamic_frame.from_options(
    frame=CustomerConsentWithACCData_node1709769943282,
    connection_type="s3",
    format="json",
    connection_options={
        "path": "s3://stedi-step-trainer-lakehouse/customer/curated/",
        "partitionKeys": [],
    },
    transformation_ctx="CustomerCurated_node1709770010699",
)

job.commit()
